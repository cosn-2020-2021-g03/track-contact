const CircuitBreaker = require('./CircuitBreaker')

const EventTracking = () => {
    return {
        healthcheckStatus: message => {
            console.log(`Healthcheck status ${message}`);
            console.log("------------------------------");
        },
        healthcheckFailed: message => {
            console.log(`call failed ${message}`);
            console.log("------------------------------");
        }
    };
};

module.exports = EventTracking;