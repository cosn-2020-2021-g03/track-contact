const { ObjectID } = require('mongodb');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let ContactSchema = new Schema({
    familyName: { type: String, max: 100 },
    givenName: { type: String, max: 100 },
    documentType: { type: String, enum: ['CITIZEN_CARD', 'PASSPORT', 'RESIDENCE_PERMIT'] },
    documentID: { type: String, max: 100 },
    gender: { type: String, max: 50, enum: ['MALE', 'FEMALE'] },
    dateOfBirth: { type: String },
    age: { type: String },
    nationality: {
        countryISOCode: String,
        countryName: String
    },
    phone: { type: String },
    email: { type: String },
    address: { type: String },
    latitude: { type: String },
    longitude: { type: String },
    city: { type: String },
    district: { type: String },
    state: { type: String },
    status: { type: String, enum: ['ACTIVE', 'INACTIVE'], default: 'ACTIVE' },
    relationshipID: { type: String },
    relationshipContactID: { type: String },
    relationship: { type: String, enum: ['PARENT', 'GRANDPARENT', 'SIBLING', 'DESCENDANT', 'OTHER'] },
    contactRelations: { type: Object }
}, { collection: 'contact', versionKey: false });

// Export the model
module.exports = mongoose.model('Contact', ContactSchema);
