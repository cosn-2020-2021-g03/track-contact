var express = require('express');
var router = express.Router();
var contact = require('./contact');

router.use('/', contact);

module.exports = router;
