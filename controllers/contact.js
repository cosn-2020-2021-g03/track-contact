const express = require('express');
const router = express.Router();
const Contact = require('../models/contact');
const _ = require('lodash');

/**
 * Express router to connect "/api/contact/create"
 *
 * @param {Object} request
 * @param {Object} response
 */
router.post('/create', async (request, response) => {
    try {
        if (!_.has(request.body, 'relationship')) {
            throw new Error('Please enter relationship status for contact...');
        }
        const contact = new Contact(request.body);
        const { _id: contactID } = await contact.save();

        response.status(200).send({ ...request.body, contactID });
    } catch (error) {
        console.log(error);
        response.status(400).send({ error: error.message });
    }
});

/**
 * Express router to connect "/api/person/all"
 *
 * @param {Object} request
 * @param {Object} response
 */
router.get('/all', async (request, response) => {
    try {
        const contact = await Contact.find({});
        const contactParent = await Contact.find({ relationshipContactID: '' });
        const contactRelations = _.chain(contact).groupBy("relationshipID").value();

        const dataArr = [];
        _.map(contactParent, value => {
            dataArr.push(_.omit({
                contactID: value._id,
                ...value._doc,
                contactRelations: _.compact(_.map(contactRelations[`${value.relationshipID}`], data => {
                    if (data.relationshipContactID) {
                        const { _id: contactID, relationshipContactID, relationship } = _.pick(data, ['_id', 'relationshipContactID', 'relationship'])
                        return { contactID, relationshipContactID, relationship };
                    }
                }))
            }, ['_id']));
        });

        response.send(dataArr);
    } catch (error) {
        response.send(error);
    }
});

/**
 * Express router to connect "/api/person/:status"
 *
 * @param {Object} request
 * @param {Object} response
 */
router.get('/status/:status', async (request, response) => {
    try {
        const { status } = request.params;
        const findParam = { status: status.toUpperCase() };
        const contact = await Contact.find(findParam);
        const contactParent = await Contact.find({ relationshipContactID: '' });
        const contactRelations = _.chain(contact).groupBy("relationshipID").value();

        const dataArr = [];
        _.map(contactParent, value => {
            dataArr.push(_.omit({
                contactID: value._id,
                ...value._doc,
                contactRelations: _.compact(_.map(contactRelations[`${value.relationshipID}`], data => {
                    if (data.relationshipContactID) {
                        const { _id: contactID, relationshipContactID, relationship } = _.pick(data, ['_id', 'relationshipContactID', 'relationship'])
                        return { contactID, relationshipContactID, relationship };
                    }
                }))
            }, ['_id']));
        });

        response.send(dataArr);
    } catch (err) {
        response.send(err);
    }
});

/**
 * Express router to connect "/api/person/:id"
 *
 * @param {Object} request
 * @param {Object} response
 */
router.get('/:id', async (request, response) => {
    try {
        const contact = await Contact.findOne({ _id: request.params.id });

        const { _id: contactID, ...rest } = contact;
        response.send(_.omit({ contactID, ...rest._doc }, ['_id']));
    } catch (err) {
        response.send(err);
    }
});

/**
 * Express router to connect "/api/person/:id/update"
 *
 * @param {Object} request
 * @param {Object} response
 */
router.put('/:id/update', function(request, response) {
    Contact.findOneAndUpdate({ _id: request.params.id }, {$set: request.body}, function (error, contact) {
        if (error) response.send(error);

        const { _id: contactID, ...rest } = contact;
        response.send(_.omit({ contactID, ...request.body, ...rest._doc }, ['_id']));
    });
});

/**
 * Express router to connect "/api/person/:id/update"
 *
 * @param {Object} request
 * @param {Object} response
 */
router.put('/:id/eliminate', function (request, response) {
    Person.findOneAndUpdate({ contactID: request.params.id }, { $set: { status: 'INACTIVE' } }, function (error, person) {
        if (error) response.send(error);
        response.send(person);
    });
});

module.exports = router;
